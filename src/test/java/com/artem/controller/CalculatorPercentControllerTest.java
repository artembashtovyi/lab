package com.artem.controller;

import com.artem.dto.CalculationRequestDto;
import com.artem.service.CalculationService;
import com.artem.service.function.FunctionOperation;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CalculatorPercentControllerTest {

    private CalculatorPercentController calculatorController;

    @BeforeMethod
    public void setUp() {
        calculatorController = new CalculatorPercentController(new CalculationService());
    }

    @Test
    public void testPercent() {
        final var actualResult = calculatorController.percent(new CalculationRequestDto("200", "1"));
        assertEquals(FunctionOperation.PERCENT, actualResult.operation);
        assertEquals("2.0000", actualResult.result);
    }
}