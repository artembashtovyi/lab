package com.artem.controller;

import com.artem.dto.CalculationRequestDto;
import com.artem.service.CalculationService;
import com.artem.service.function.FunctionOperation;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CalculatorAddControllerTest {

    private CalculatorAddController calculatorController;

    @BeforeMethod
    public void setUp() {
        calculatorController = new CalculatorAddController(new CalculationService());
    }

    @Test
    public void testAdd() {
        final var actualResult = calculatorController.add(new CalculationRequestDto("2", "2"));
        assertEquals(FunctionOperation.ADD, actualResult.operation);
        assertEquals("4.0000", actualResult.result);
    }
}