package com.artem.controller;

import com.artem.dto.CalculationRequestDto;
import com.artem.service.CalculationService;
import com.artem.service.function.FunctionOperation;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CalculatorMultiplyControllerTest {

    private CalculatorMultiplyController calculatorController;

    @BeforeMethod
    public void setUp() {
        calculatorController = new CalculatorMultiplyController(new CalculationService());
    }

    @Test
    public void testAdd() {
        final var actualResult = calculatorController.multiply(new CalculationRequestDto("2", "2"));
        assertEquals(FunctionOperation.MULTIPLY, actualResult.operation);
        assertEquals("4.0000", actualResult.result);
    }
}