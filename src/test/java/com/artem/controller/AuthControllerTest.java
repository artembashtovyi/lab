package com.artem.controller;


import com.artem.dto.LoginDto;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class AuthControllerTest {
    private AuthController authController;

    @BeforeMethod
    public void setUp() {
        authController = new AuthController();
    }

    @Test
    public void testLogin() {
        final var actualResult = authController.login(new LoginDto("admin", "admin"));
        assertNotNull(actualResult.sessionId);
    }

    @Test(expectedExceptions = RuntimeException.class)
    public void testLoginWhenCredentialsAreNotValid() {
        authController.login(new LoginDto(null, "admin"));
    }
}
