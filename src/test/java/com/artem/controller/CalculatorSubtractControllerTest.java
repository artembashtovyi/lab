package com.artem.controller;

import com.artem.dto.CalculationRequestDto;
import com.artem.service.CalculationService;
import com.artem.service.function.FunctionOperation;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CalculatorSubtractControllerTest {

    private CalculatorSubtractController calculatorController;

    @BeforeMethod
    public void setUp() {
        calculatorController = new CalculatorSubtractController(new CalculationService());
    }

    @Test
    public void testAdd() {
        final var actualResult = calculatorController.subtract(new CalculationRequestDto("2", "2"));
        assertEquals(FunctionOperation.SUBTRACT, actualResult.operation);
        assertEquals("0.0000", actualResult.result);
    }
}