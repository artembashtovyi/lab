package com.artem.controller;

import com.artem.dto.CalculationRequestDto;
import com.artem.exception.CalculationException;
import com.artem.service.CalculationService;
import com.artem.service.function.FunctionOperation;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CalculatorDivideControllerTest {

    private CalculatorDivideController calculatorController;

    @BeforeMethod
    public void setUp() {
        calculatorController = new CalculatorDivideController(new CalculationService());
    }

    @Test
    public void testDivide() {
        final var actualResult = calculatorController.divide(new CalculationRequestDto("4", "2"));
        assertEquals(FunctionOperation.DIVIDE, actualResult.operation);
        assertEquals("2.0000", actualResult.result);
    }

    @Test(expectedExceptions = CalculationException.class)
    public void testDivideWhenArgumentIsZero() {
        final var actualResult = calculatorController.divide(new CalculationRequestDto("4", "0"));
        assertEquals(FunctionOperation.DIVIDE, actualResult.operation);
        assertEquals("2.000", actualResult.result);
    }
}