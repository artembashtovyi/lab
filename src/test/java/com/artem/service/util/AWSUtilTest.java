package com.artem.service.util;

import org.testng.annotations.*;

import static org.testng.Assert.assertEquals;

public class AWSUtilTest {

    @DataProvider(name = "arnDp")
    public static Object[][] data() {
        return new Object[][]{
                {null, "ec2", "org_123", "/instance", "arn::ec2:org_123:/instance"},
                {"", "ec2", "org_123", "/instance", "arn::ec2:org_123:/instance"},
                {"", "ec2", "org_123", "/instance/${instanceName}", "arn::ec2:org_123:/instance/${instanceName}"},
                {"", "dynamodb", "org_123", "/dynamodb/${tableName}/property/${propertyName}",
                        "arn::dynamodb:org_123:/dynamodb/${tableName}/property/${propertyName}"
                }
        };
    }

    @Test(dataProvider = "arnDp")
    public void testConstructFrn(String client, String resource, String orgId, String path, String expectedUrl) {
        assertEquals(expectedUrl, AWSUtil.generateArn(client, resource, orgId, path));
    }

}