package com.artem.service.util;

import com.artem.exception.HexDecodeException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class HexDecodeUtilTest {

    @DataProvider(name = "validHexDp", parallel = true)
    public Object[][] validHex() {
        return new Object[][]{
                {
                        "00112233445566778899aabbccddeeff", new byte[]{(byte) 0, 17, 34, 51, 68, 85, 102, 119, -120, -103, -86, -69, -52, -35, -18, -1}
                },
                {
                        "00112233445566778899AABBCCDDEEFF", new byte[]{(byte) 0, 17, 34, 51, 68, 85, 102, 119, -120, -103, -86, -69, -52, -35, -18, -1}
                }
        };
    }

    @DataProvider(name = "invalidHexDp", parallel = true)
    public Object[][] invalidHex() {
        return new Object[][]{
                {"aa1"},
                {"Invalid"}

        };
    }


    @Test(dataProvider = "validHexDp")
    public void testConvertToBytesWhenValueIsNotValid(String value, byte[] expectedValue) {
        final byte[] actualResult = HexDecodeUtil.convertToBytes(value);
        assertEquals(expectedValue, actualResult);
    }

    @Test(dataProvider = "invalidHexDp", expectedExceptions = {HexDecodeException.class})
    public void testConvertToBytesWhenValueIsNotValid(String value) {
        HexDecodeUtil.convertToBytes(value);
    }
}
