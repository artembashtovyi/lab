package com.artem.service;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.testng.Assert.assertEquals;

public class EndpointMatcherTest {
    private static final Set<String> ENDPOINTS = new HashSet<>(Arrays.asList("/asset",
            "/asset/${assetName}", "/asset/${assetName}/template/${templateName}", "/asset/template/${templateName}"));
    private EndpointMatcher endpointParser;

    @DataProvider(name = "endpointDp", parallel = true)
    public Object[][] additionFunction() {
        return new Object[][]{
                {"/asset", "/asset"},
                {"/asset/${assetName}", "/asset/MF-101"},
                {"/asset/${assetName}/template/${templateName}", "/asset/MF-101/template/TL-101"},
                {"/asset/template/${templateName}", "/asset/template/TL-101"},
        };
    }

    @BeforeClass
    public void setUp() {
        endpointParser = new PathVariableEndpointMatcher();
    }

    @Test(dataProvider = "endpointDp")
    public void testMatchPathVariableEndpoint(String expectedEndpoint, String path) {
        String actualEndpoint = endpointParser.matchEndpoint(ENDPOINTS, path);
        assertEquals(expectedEndpoint, actualEndpoint);
    }
}

