package com.artem.service;

import com.artem.dto.CalculationRequestDto;
import com.artem.dto.CalculationResultDto;
import com.artem.exception.CalculationException;
import com.artem.service.function.FunctionOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CalculationServiceTest {
    private static final Logger LOGGER = LogManager.getLogger(CalculationServiceTest.class);

    private CalculationService calculationService;

    @BeforeClass
    public void beforeClass() {
        calculationService = new CalculationService();
    }

    @DataProvider(name = "additionFunction", parallel = true)
    public Object[][] additionFunction() {
        return new Object[][]{
                {new CalculationRequestDto("10.01", "1.01"), new CalculationResultDto("11.0200", FunctionOperation.ADD)},
                {new CalculationRequestDto("-1.02", "1.02"), new CalculationResultDto("0.0000", FunctionOperation.ADD)},
                {new CalculationRequestDto("-1", "0"), new CalculationResultDto("-1.0000", FunctionOperation.ADD)},
        };
    }

    @DataProvider(name = "multiplicationFunction", parallel = true)
    public Object[][] multiplyFunction() {
        return new Object[][]{
                {new CalculationRequestDto("2", "2"), new CalculationResultDto("4.0000", FunctionOperation.MULTIPLY)},
                {new CalculationRequestDto("-1", "1.02"), new CalculationResultDto("-1.0200", FunctionOperation.MULTIPLY)},
                {new CalculationRequestDto("-1", "0"), new CalculationResultDto("0.0000", FunctionOperation.MULTIPLY)},
        };
    }

    @DataProvider(name = "divisionFunction", parallel = true)
    public Object[][] divisionFunction() {
        return new Object[][]{
                {new CalculationRequestDto("2", "2"), new CalculationResultDto("1.0000", FunctionOperation.DIVIDE)},
                {new CalculationRequestDto("4", "2"), new CalculationResultDto("2.0000", FunctionOperation.DIVIDE)},
                {new CalculationRequestDto("0", "-1"), new CalculationResultDto("0.0000", FunctionOperation.DIVIDE)},
        };
    }

    @DataProvider(name = "subtractFunction", parallel = true)
    public Object[][] subtractFunction() {
        return new Object[][]{
                {new CalculationRequestDto("10", "-5"), new CalculationResultDto("15.0000", FunctionOperation.SUBTRACT)},
                {new CalculationRequestDto("-4", "-2"), new CalculationResultDto("-2.0000", FunctionOperation.SUBTRACT)},
                {new CalculationRequestDto("-1", "0"), new CalculationResultDto("-1.0000", FunctionOperation.SUBTRACT)},
        };
    }

    @Test(dataProvider = "additionFunction")
    public void testAdd(CalculationRequestDto givenRequest, CalculationResultDto expectedResult) {
        final var actualResult = calculationService.add(givenRequest);
        assertCalculationResult(expectedResult, actualResult);
    }

    @Test(dataProvider = "multiplicationFunction")
    public void testMultiply(CalculationRequestDto givenRequest, CalculationResultDto expectedResult) {
        final var actualResult = calculationService.multiply(givenRequest);
        assertCalculationResult(expectedResult, actualResult);
    }

    @Test(dataProvider = "divisionFunction")
    public void testDivide(CalculationRequestDto givenRequest, CalculationResultDto expectedResult) {
        final var actualResult = calculationService.divide(givenRequest);
        assertCalculationResult(expectedResult, actualResult);
    }

    @Test(dataProvider = "subtractFunction")
    public void testSubtract(CalculationRequestDto givenRequest, CalculationResultDto expectedResult) {
        final var actualResult = calculationService.subtract(givenRequest);
        assertCalculationResult(expectedResult, actualResult);
    }

    @Test
    public void testPercent() {
        var givenRequest = new CalculationRequestDto("200", "50");
        final var percent = calculationService.percent(givenRequest);
        assertEquals(percent.operation, FunctionOperation.PERCENT);
        assertEquals(percent.result, "100.0000");
    }

    @Test(dependsOnMethods = "testSubtract", expectedExceptions = CalculationException.class)
    public void testAddWhenNumbersAreNotValid() {
        var givenRequest = new CalculationRequestDto("asd", "123");
        calculationService.subtract(givenRequest);
    }

    @AfterClass
    public void afterClass() {
        calculationService = null;
        LOGGER.info("Finish calculation service test");
    }

    private void assertCalculationResult(CalculationResultDto expectedResult, CalculationResultDto actualResult) {
        assertEquals(actualResult.result, expectedResult.result);
        assertEquals(actualResult.operation, expectedResult.operation);
    }
}
