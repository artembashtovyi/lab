package com.artem.controller;

import com.artem.dto.CalculationRequestDto;
import com.artem.dto.CalculationResultDto;
import com.artem.service.CalculationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public class CalculatorPercentController {
    private static final Logger LOGGER = LogManager.getLogger(CalculatorPercentController.class);

    private final CalculationService service;

    public CalculatorPercentController(CalculationService service) {
        this.service = service;
    }

    @PostMapping
    public CalculationResultDto percent(@RequestBody CalculationRequestDto request) {
        LOGGER.info("User request={}", request);
        return service.percent(request);
    }
}
