package com.artem.controller;

import com.artem.dto.EmployeeDto;
import com.artem.dto.UserNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/employee")
public class EmployeeController {
    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);

    @GetMapping("/{id}")
    public EmployeeDto getEmployee(@PathVariable String id) {
        LOGGER.info("Get employee {}", id);
        return findEmployee(id);
    }

    private EmployeeDto findEmployee(String id) {
        if ("1".equals(id))
            return new EmployeeDto("1", "emp1", "manager", 300D);
        throw new UserNotFoundException();
    }
}
