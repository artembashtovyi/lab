package com.artem.controller;

import com.artem.dto.CalculationRequestDto;
import com.artem.dto.CalculationResultDto;
import com.artem.service.CalculationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/add")
public class CalculatorAddController {
    private static final Logger LOGGER = LogManager.getLogger(CalculatorAddController.class);

    private final CalculationService service;

    public CalculatorAddController(CalculationService service) {
        this.service = service;
    }

    @PostMapping
    public CalculationResultDto add(@RequestBody CalculationRequestDto request) {
        LOGGER.info("User request={}", request);
        return service.add(request);
    }
}
