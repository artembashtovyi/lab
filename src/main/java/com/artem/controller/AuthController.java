package com.artem.controller;

import com.artem.dto.LoginDto;
import com.artem.dto.LoginResponseDto;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/auth")
public class AuthController {

    private static final String LOGIN = "admin";
    private static final String PASSWORD = "admin";

    public AuthController() {
    }

    @PostMapping
    public LoginResponseDto login(@RequestBody LoginDto loginDto) {
        userLogin(loginDto);
        return new LoginResponseDto(UUID.randomUUID().toString());
    }

    private void userLogin(LoginDto loginDto) {
        if (!LOGIN.equals(loginDto.username)) {
            throw new RuntimeException("User name doesn't match");
        }
        if (!PASSWORD.equals(loginDto.password)) {
            throw new RuntimeException("Password doesn't match");
        }
    }
}


