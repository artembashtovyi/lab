package com.artem.service;

import javax.annotation.Nullable;
import java.util.Set;

public interface EndpointMatcher {
    @Nullable
    String matchEndpoint(Set<String> endpoints, String path);
}
