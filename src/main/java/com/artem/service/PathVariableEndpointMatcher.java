package com.artem.service;

import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Set;
import java.util.stream.Collectors;

@Component
class PathVariableEndpointMatcher implements EndpointMatcher {
    private static final String VARIABLE_INDICATOR = "${";
    private static final String PATH_DELIMITER = "/";

    public PathVariableEndpointMatcher() {
    }

    private boolean containsPathVariable(@Nonnull String endpoint) {
        return endpoint.contains(VARIABLE_INDICATOR);
    }


    // Matches path to endpoint
    // E.g. path == /assets/MF-104 => /assets/${assetName}
    @Nullable
    @Override
    public String matchEndpoint(Set<String> endpoints, String path) {
        String matchEndpoint = matchSimpleEndpoint(endpoints, path);
        if (matchEndpoint != null) {
            return matchEndpoint;
        }
        final Set<String> pathVariableEndpoints = endpoints.stream()
                .filter(this::containsPathVariable)
                .collect(Collectors.toSet());
        return matchEndpointWithPathVariable(pathVariableEndpoints, path);
    }


    private String matchSimpleEndpoint(Set<String> endpoints, String path) {
        if (Strings.isBlank(path)) {
            return null;
        }
        return endpoints.contains(path) ? path : null;
    }

    private String matchEndpointWithPathVariable(Set<String> endpoints, String path) {
        final String[] pathParts = path.split(PATH_DELIMITER);
        return endpoints.stream()
                .filter(endpoint -> isPathEqualsEndpointWithPathVariable(pathParts, endpoint.split(PATH_DELIMITER)))
                .findAny()
                .orElse(null);
    }

    private boolean isPathEqualsEndpointWithPathVariable(String[] path, String[] endpoint) {
        if (endpoint.length != path.length) {
            return false;
        }
        for (int i = 0; i < endpoint.length; i++) {
            if (endpoint[i].startsWith(VARIABLE_INDICATOR)) {
                continue;
            }
            if (!endpoint[i].equals(path[i])) return false;
        }
        return true;
    }
}
