package com.artem.service;

import com.artem.dto.CalculationRequestDto;
import com.artem.dto.CalculationResultDto;
import com.artem.exception.CalculationException;
import com.artem.service.function.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
public class CalculationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CalculationService.class);

    public CalculationResultDto add(CalculationRequestDto calculationRequest) {
        LOGGER.info("Add operation");
        return calculate(new AdditionFunction(), calculationRequest);
    }

    public CalculationResultDto multiply(CalculationRequestDto calculationRequest) {
        LOGGER.info("Multiply operation");
        return calculate(new MultiplicationFunction(), calculationRequest);
    }

    public CalculationResultDto subtract(CalculationRequestDto calculationRequest) {
        LOGGER.info("Subtract operation");
        return calculate(new SubtractionFunction(), calculationRequest);
    }

    public CalculationResultDto divide(CalculationRequestDto calculationRequest) {
        LOGGER.info("Divide operation");
        return calculate(new DivisionFunction(), calculationRequest);
    }

    public CalculationResultDto percent(CalculationRequestDto calculationRequest) {
        LOGGER.info("Percent operation");
        return calculate(new PercentFunction(), calculationRequest);
    }

    private CalculationResultDto calculate(CalculationFunction function, CalculationRequestDto request) {
        try {
            final var firstNumber = new BigDecimal(request.firstNumber);
            final var secondNumber = new BigDecimal(request.secondNumber);
            final var result = function.execute(firstNumber, secondNumber).setScale(4, RoundingMode.HALF_EVEN);
            return new CalculationResultDto(result.toString(), function.getOperation());
        } catch (NumberFormatException | ArithmeticException e) {
            LOGGER.error("[{}] function error", function.getOperation(), e);
            throw new CalculationException(e.getMessage(), function.getOperation(), request.firstNumber,
                    request.secondNumber);
        }
    }
}
