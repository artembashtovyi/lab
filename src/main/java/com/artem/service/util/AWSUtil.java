package com.artem.service.util;

import javax.annotation.Nullable;

public class AWSUtil {
    private static final StringBuilder BUILDER = new StringBuilder();

    private AWSUtil() {
    }

    public static String generateArn(@Nullable String client, @Nullable String resource, @Nullable String orgId,
                                     String path) {
        BUILDER.setLength(0);
        return BUILDER.append("arn:")
                .append(defaultString(client)).append(":")
                .append(defaultString(resource)).append(":")
                .append(defaultString(orgId)).append(":")
                .append(path).toString();
    }

    private static String defaultString(String line) {
        if (line == null || line.isBlank()) {
            return "";
        }
        return line;
    }

}
