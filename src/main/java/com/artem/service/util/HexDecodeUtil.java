package com.artem.service.util;

import com.artem.exception.HexDecodeException;
import org.bouncycastle.util.encoders.DecoderException;
import org.bouncycastle.util.encoders.Hex;

public class HexDecodeUtil {
    public static byte[] convertToBytes(String param) {
        try {
            return Hex.decode(param);
        } catch (DecoderException e) {
            throw new HexDecodeException("Hex parameter is not valid: " + param, e);
        }
    }
}
