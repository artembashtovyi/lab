package com.artem.service.function;

import javax.annotation.Nonnull;
import java.math.BigDecimal;

public interface CalculationFunction {
    @Nonnull
    FunctionOperation getOperation();
    BigDecimal execute(BigDecimal firstNumber, BigDecimal secondNumber) throws ArithmeticException;
}
