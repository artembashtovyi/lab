package com.artem.service.function;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class PercentFunction implements CalculationFunction {
    private static final BigDecimal ONE_HUNDRED = new BigDecimal(100);

    @Nonnull
    @Override
    public FunctionOperation getOperation() {
        return FunctionOperation.PERCENT;
    }

    @Override
    public BigDecimal execute(BigDecimal firstNumber, BigDecimal secondNumber) throws ArithmeticException {
        return firstNumber.multiply(secondNumber).divide(ONE_HUNDRED, RoundingMode.HALF_EVEN);
    }
}
