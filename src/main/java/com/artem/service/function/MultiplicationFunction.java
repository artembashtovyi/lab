package com.artem.service.function;

import javax.annotation.Nonnull;
import java.math.BigDecimal;

public class MultiplicationFunction implements CalculationFunction {

    @Nonnull
    @Override
    public FunctionOperation getOperation() {
        return FunctionOperation.MULTIPLY;
    }

    @Override
    public BigDecimal execute(BigDecimal firstNumber, BigDecimal secondNumber) {
        return firstNumber.multiply(secondNumber);
    }
}
