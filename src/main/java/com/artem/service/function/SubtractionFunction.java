package com.artem.service.function;

import javax.annotation.Nonnull;
import java.math.BigDecimal;

public class SubtractionFunction implements CalculationFunction {
    @Nonnull
    @Override
    public FunctionOperation getOperation() {
        return FunctionOperation.SUBTRACT;
    }

    @Override
    public BigDecimal execute(BigDecimal firstNumber, BigDecimal secondNumber) throws ArithmeticException {
        return firstNumber.subtract(secondNumber);
    }
}
