package com.artem.service.function;

import javax.annotation.Nonnull;
import java.math.BigDecimal;

public class AdditionFunction implements CalculationFunction {

    @Nonnull
    @Override
    public FunctionOperation getOperation() {
        return FunctionOperation.ADD;
    }

    @Override
    public BigDecimal execute(BigDecimal firstNumber, BigDecimal secondNumber) {
        return firstNumber.add(secondNumber);
    }
}
