package com.artem.service.function;

public enum FunctionOperation {
    ADD,
    MULTIPLY,
    SUBTRACT,
    DIVIDE,
    PERCENT
}
