package com.artem.service.function;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class DivisionFunction implements CalculationFunction {
    @Nonnull
    @Override
    public FunctionOperation getOperation() {
        return FunctionOperation.DIVIDE;
    }

    @Override
    public BigDecimal execute(BigDecimal firstNumber, BigDecimal secondNumber) throws ArithmeticException {
        return firstNumber.divide(secondNumber, RoundingMode.HALF_EVEN);
    }
}
