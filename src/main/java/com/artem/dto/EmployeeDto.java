package com.artem.dto;

public class EmployeeDto {
    public final String empId;
    public final String name;
    public final String designation;
    public final double salary;

    public EmployeeDto(String empId, String name, String designation, double salary) {
        this.empId = empId;
        this.name = name;
        this.designation = designation;
        this.salary = salary;
    }
}
