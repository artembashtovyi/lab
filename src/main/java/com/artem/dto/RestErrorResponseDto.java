package com.artem.dto;

public class RestErrorResponseDto {

    public final String message;

    public RestErrorResponseDto(String message) {
        this.message = message;
    }
}
