package com.artem.dto;

public class CalculationRequestDto {
    public final String firstNumber;
    public final String secondNumber;

    public CalculationRequestDto(String firstNumber, String secondNumber) {
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
    }

    @Override
    public String toString() {
        return "{" +
                "firstNumber='" + firstNumber + '\'' +
                ", secondNumber='" + secondNumber + '\'' +
                '}';
    }
}
