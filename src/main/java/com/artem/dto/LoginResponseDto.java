package com.artem.dto;

public class LoginResponseDto {
    public final String sessionId;

    public LoginResponseDto(String sessionId) {
        this.sessionId = sessionId;
    }
}
