package com.artem.dto;

import com.artem.service.function.FunctionOperation;

public class CalculationResultDto {
    public final String result;
    public final FunctionOperation operation;

    public CalculationResultDto(String result, FunctionOperation operation) {
        this.result = result;
        this.operation = operation;
    }
}
