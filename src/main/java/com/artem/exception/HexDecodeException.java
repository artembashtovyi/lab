package com.artem.exception;

public class HexDecodeException extends RuntimeException {
    public HexDecodeException(String message, Throwable cause) {
        super(message, cause);
    }
}
