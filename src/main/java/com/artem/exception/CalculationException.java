package com.artem.exception;

import com.artem.service.function.FunctionOperation;

public class CalculationException extends RuntimeException {
    public final FunctionOperation operation;
    public final String firstNumber;
    public final String secondNumber;

    public CalculationException(String message, FunctionOperation operation, String firstNumber, String secondNumber) {
        super(message);
        this.operation = operation;
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
    }
}
