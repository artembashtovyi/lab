package com.artem.advice;

import com.artem.dto.RestErrorResponseDto;
import com.artem.dto.UserNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GeneralExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(GeneralExceptionHandler.class);

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<RestErrorResponseDto> handle(UserNotFoundException e) {
        LOGGER.error("User does not exist", e);
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(new RestErrorResponseDto("Menu does not exist"));
    }
}
